// No hotel morte lenta, estavam hospedados 5 hospédes, Renato Faca, Maria Cálice, Roberto Arma, Roberta Corda e Uesllei.
// A meia noite, o hóspede do quarto 101 foi encontrado morto e os 5 suspeitos são os citados acima.
// Descubra quem é o assassino sabendo que:
// Se o nome do suspeito contem 3 vogais ou mais(Nome completo) e nenhuma dessas vogais é u ou o, ele pode ser o assassino.
// Se o suspeito for do sexo feminino, só sera assassina se tiver utilizado uma arma branca.
// Se o suspeito for do sexo masculino, ele deveria estar no saguão do hotel a 00h30 para ser considerado o assasino.
// Sabendo dessas informações acima, projete o algorítmo que fornecido o nome do suspeito, indique se ele pode ou não ser o assassino.

package main

import ("fmt"
		"strings"
		"time")

func main() {

	// solcitar nome ao usuário
	name := askString("Nome: ")

	// solcitar nome ao usuário
	sobre := askString("Sobrenome: ")

	// solcitar genero ao user
	gender := askString("Genero: ")

	// verifica se o nome é uma string
	vn := checkString(name, "1234567890!@#$%*()<>.,:;?/")

	vs := checkString(sobre, "1234567890!@#$%*()<>.,:;?/")

	vg := checkString(gender, "1234567890!@#$%*()<>.,:;?/")

	if !vn || !vs || !vg {
		fmt.Println("Não sabe nem o que é um nome")
		time.Sleep(3 * time.Second)
		return
	}

	fmt.Println("Seja bem-vindo", name, sobre, "do gênero biológico", gender)
	
	// verifica se o nome contains 'O' ou 'U' e se tem mais de 3 'A' ou 'E' ou 'I'
	vv := checkString(name, "ou") && (aei_counter(name) >= 3)

	if !vv {
		fmt.Println("Não o que vem a seguir: Suspeito! Forte! Foi você!")
		time.Sleep(3 * time.Second)
		return
	}

	weapon_test := false
	time_test := true

	if gender == "feminino" {
		weapon := askString("Possui algum tipo de arma de fogo? ")
		weapon_test = str_to_boolean(weapon)
	}
	
	if gender == "masculino" {
		time := askString("Estava no saguão do hotel às 00:30? ")
		time_test = str_to_boolean(time)
	}

	if weapon_test || !time_test {
		fmt.Println("Não o que vem a seguir: Suspeito! Forte! Foi você!")
		time.Sleep(3 * time.Second)
		return
	}
	fmt.Println("ASSSSAAASSSIIINO(AAA)!!!!!!!!!!!!!!!!!!!!!!!!111")
	time.Sleep(3 * time.Second)
	return
}

func askString(msg string) string {

	// declara a var input
	var input string

	// apresenta a string Nome on-the-face
	fmt.Print(msg)
    
    // lê o que o usuário digitou
    fmt.Scanln(&input)

    return input
}

func checkString(s string, chars string) bool {
	s = strings.ToLower(s)
	return !strings.ContainsAny(s, chars)
}
func aei_counter(s string) int{
	s = strings.ToLower(s)
	counter := strings.Count(s, "a")
	counter += strings.Count(s, "e")
	counter += strings.Count(s, "i")
	return counter
}

func str_to_boolean(s string) bool{
	s = strings.ToLower(s)
	if s == "sim" || s == "s" || s == "si" || s == "yes" {
		return true
	}
	return false
}