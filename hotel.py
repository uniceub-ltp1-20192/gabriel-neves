""" No hotel morte lenta, estavam hospedados 5 hospédes, Renato Faca, Maria Cálice, Roberto Arma, Roberta Corda e Uesllei.
A meia noite, o hóspede do quarto 101 foi encontrado morto e os 5 suspeitos são os citados acima.
Descubra quem é o assassino sabendo que:
Se o nome do suspeito contem 3 vogais ou mais(Nome completo) e nenhuma dessas vogais é u ou o, ele pode ser o assassino.
Se o suspeito for do sexo feminino, só sera assassina se tiver utilizado uma arma branca.
Se o suspeito for do sexo masculino, ele deveria estar no saguão do hotel a 00h30 para ser considerado o assasino.
Sabendo dessas informações acima, projete o algoritmo que fornecido o nome do suspeito, indique se ele pode ou não ser o assassino."""

import sys


def askName():
    return input("Nome: ")


def validateString(s):
    return isinstance(s, str)


def validateVogals(s):
    s.lower()

    aei_number = s.count("a")
    aei_number += s.count("e")
    aei_number += s.count("i")

    ou_number = s.count("o")
    ou_number += s.count("u")

    return (aei_number >= 3) and (ou_number == 0)


names = ["Renato Faca", "Maria Cálice", "Roberto Arma", "Roberta Corda", "Uesllei"]


def getBioGender(s):
    genders = ["male", "female", "male", "female", "male"]
    return genders[names.index(s)]


def getWeapon(s):
    weapons = ["faca", "cálice", "arma", "corda", "mão"]
    return weapons[names.index(s)]


def getTime(s):
    hour = ["10:00", "11:00", "6:00", "3:00", "12:30"]
    return hour[names.index(s)]


def validateAssassinWoman(gender, wea):
    return (gender == "female") and (wea == "cálice" or wea == "corda" or wea == "mão")


def validateAssassinMan(gender, hour):
    return (gender == "male") and (hour == "12:00")


# solicitar nome ao usuário
name = askName()

# verifica se o nome é uma string
vn = validateString(name)

if not vn:
    sys.exit("Não sabe nem o que é um nome")

# verifica se nome respeita a RN1 (vogais)
vv = validateVogals(name)

if not vv:
    sys.exit("Não o que vem a seguir: Suspeito! Forte! Foi você!")

biogender = getBioGender(name)
weapon = getWeapon(name)
time = getTime(name)

vw = validateAssassinWoman(biogender, weapon)
vm = validateAssassinMan(biogender, time)

if not(vw and vm):
    sys.exit("Não o que vem a seguir: Suspeito! Forte! Foi você!")

print("ASSSSAAASSSIIINO(AAA)!!!!!!!!!!!!!!!!!!!!!!!!111")
