from random import shuffle

def ask_yes_or_no(text):
    result = input(text)
    affirmative = ["sim", "s", "si", "yes", "y", "ye", ""]
    if result.lower() in affirmative:
        return True

    return False

step_by_step = ask_yes_or_no("\nDeseja rodar passo-a-passo(dando Enter)? ")

SUITS          = ("SPADES", "HEARTS", "DIAMONDS", "CLUBS")
CARD_MAX_VALUE = 13
CARD_MIN_VALUE = 1
print("\n")


def generateCardDeck():
	
	cards = []

	for suit in SUITS:
		for value in range(CARD_MIN_VALUE, CARD_MAX_VALUE + 1):
			card = (suit, value)
			cards.append(card)

	return cards

def getTrucoDeck():
	deck = generateCardDeck()
	notValidCards = [8, 9, 10]
	trucoDeck = []

	for i in deck:
		if not( i[1] in notValidCards ):
			trucoDeck.append(i)

	trucoDeck.append("JOKER") # Porque jogo com coringa...
	
	return trucoDeck

def generateShuffleDeck():
	deck = getTrucoDeck()
	shuffle(deck)
	return deck

def deliverCards():
	gameDeck = generateShuffleDeck()
	return gameDeck[0:3], gameDeck[3:6], gameDeck[6:9], gameDeck[9:12]

def cardsRanking(card): # quanto menor o valor, mais forte
	
	manilha  = (("CLUBS", 4), ("HEARTS", 7), ("SPADES", 1), ("DIAMONDS", 7), "JOKER")
	valueOrder = (3, 2, 1, 13, 11, 12, 7, 6, 5, 4)

	if card in manilha:
		return manilha.index(card)

	return valueOrder.index(card[1]) + 5

def smallest(a, b):
	if a < b:
		return a
	
	return b

def compareCards(p1, p2, p3, p4): # vence o menor valor
	c1, c2, c3, c4 = cardsRanking(p1), cardsRanking(p2), cardsRanking(p3), cardsRanking(p4)

	team1_3 = smallest(c1, c3)
	team2_4 = smallest(c2, c4)
	
	print("Mão:\n\tTime 1 - ", p1, p3, "\n\tTime 2 - ", p2, p4)
	if team1_3 < team2_4:
		print("Vitória do time 1\n")
		return 1
	
	if team1_3 > team2_4:
		print("Vitória do time 2\n")
		return 2
	
	print("Empate\n")
	return 0 # empate

def trucoHands(p1, p2, p3, p4):
	team1_3, team2_4 = 0, 0

	for i in range(3):
		hand = compareCards(p1[i], p2[i], p3[i], p4[i])
		
		if hand == 1:
			team1_3 += 1
		elif hand == 2:
			team2_4 += 1

	if team1_3 >= 2:
		return 1
	
	if team2_4 >= 2:
		return 2
	
	return 0

def trucoRound():
	p1, p2, p3, p4 = deliverCards()

	handsResult = trucoHands(p1, p2, p3, p4)

	if handsResult == 1:
		return 1

	if handsResult == 2:
		return 2

	return 0

def pauseForEnter():
	if step_by_step:
		return input("")
	return

def game():
	winningPoint = 12
	team1_3, team2_4 = 0, 0

	while team1_3 < winningPoint and team2_4 < winningPoint:
		print("\n- Rodada:\n")
		roundResult = trucoRound()

		if roundResult == 1:
			team1_3 += 1

		elif roundResult == 2:
			team2_4 += 1

		else:
			print("Rodada empatada.")

		print("\n\nPlacar:\tTime 1\t\tTime 2\n\t ", team1_3, "\t\t ", team2_4, "\n\n")
		pauseForEnter()
	return (team1_3, team2_4)

def announceWinner(result):
	if result[0] > result[1]:
		print("Time 1 venceu por", result[0], "a", result[1], "pontos!")
		return

	print("Time 2 venceu por", result[1], "a", result[0], "pontos!")
	return


result = game()

announceWinner(result)