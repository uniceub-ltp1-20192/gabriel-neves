cubos = [num**3 for num in range(1, 101)]

print("Os primeiros 3 itens da lista são:", cubos[:3])
print("Os últimos 3 itens da lista são:", cubos[-3:])
meio = int(len(cubos)/2)
print("Os 3 itens no meio da lista são:", cubos[meio-1:meio+2])
