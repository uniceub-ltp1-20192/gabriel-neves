cebolas = 1584
cebolas_na_caixa = 749
espaco_caixa = 7
caixas = 156
cebolas_fora_da_caixa = cebolas - cebolas_na_caixa
caixas_vazias = caixas - (cebolas_na_caixa/espaco_caixa)
caixas_necessarias = cebolas_fora_da_caixa / espaco_caixa

print("Existem", cebolas_na_caixa, "cebolas encaixotadas")
print("Existem", cebolas_fora_da_caixa, "cebolas sem caixa")
print("Em cada caixa cabem", espaco_caixa, "cebolas")
print("Ainda temos,", caixas_vazias, "caixas vazias")
print("Então, precisamos de", caixas_necessarias, "caixas para empacotar todas as cebolas")