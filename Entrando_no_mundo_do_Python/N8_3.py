linguagens = ["C++", "Python", "Ruby", "Go", "JavaScript"]

print(sorted(linguagens))

linguagens.sort(reverse=True)
print(linguagens)

linguagens.reverse()
print(linguagens)

print("\nHá", len(linguagens), "itens nesta lista.")