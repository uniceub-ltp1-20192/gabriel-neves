number = int(input("Informe um número inteiro: "))
resto = number % 2

if resto == 0:
	print(str(number) + " é um número par")

else:
	print(str(number) + " é um número ímpar")