def convite(lista):
	for i in lista:
		print("Ei", i+", gostaria de jantar comigo?")

names = ["Renata Faca", "Robson Arma", "Ueslley"]

convite(names)

print("\nQue pena! Parece que", names[2], "não poderá comparecer.\n")

del names[2]
names.append("Maria Cálice")

convite(names)

print("\nConsegui uma mesa maior para o jantar!\n")

names.insert(0, "irmã do Ueslley")
names.insert(2, "Dougras")
names.append("vó da Maria")

convite(names)