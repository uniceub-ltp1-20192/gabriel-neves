cardapio = ("macarrão", "pizza", "hamburguer", "pipoca", "sorvete")

for comida in cardapio:
	print(comida)

try:
	cardapio[-2] = "churrasco" 
	cardapio[-1] = "feijoada"

except TypeError:
	cardapio = ("macarrão", "pizza", "hamburguer", "churrasco", "feijoada")

print("\n")
for comida in cardapio:
	print(comida)	