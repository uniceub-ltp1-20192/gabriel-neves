def convite(lista):
	for i in lista:
		print("Ei", i+", gostaria de jantar comigo?")

def excluir_da_lista(lista, i):
	expulso = lista.pop(i)
	print("Infelizmente, não poderei te convidar,", expulso)

names = ["Renata Faca", "Robson Arma", "Ueslley"]

convite(names)

print("\nQue pena! Parece que", names[2], "não poderá comparecer.\n")

del names[2]
names.append("Maria Cálice")

convite(names)

print("\nConsegui uma mesa maior para o jantar!\n")

names.insert(0, "irmã do Ueslley")
names.insert(2, "Dougras")
names.append("vó da Maria")

convite(names)

print("\nPoderei convidar apenas duas pessoas para o jantar.\n")

for i in range(len(names)-1, 1, -1):
	excluir_da_lista(names, i)

for i in names:
	print("Você ainda está convidado(a),", i)

del names[1]
del names[0]

print(names)