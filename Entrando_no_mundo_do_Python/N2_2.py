cebolas = 300 # atribui o valor inteiro 300 à variável cabolas
cebolas_na_caixa = 120 # atribui o valor inteiro 120 à variável cabolas_na_caixa
espaco_caixa = 5 # atribui o valor inteiro 5 à variável espaco_caixa
caixas = 60 # atribui o valor inteiro 60 à variável caixas
cebolas_fora_da_caixa = cebolas - cebolas_na_caixa # atribui à variável cabolas_fora_da_caixa o resultado da subtração entre cebolas e cebolas_na_caixa
caixas_vazias = caixas - (cebolas_na_caixa/espaco_caixa) # atribui à variável caixas_vazias o resultado da subtração de caixas pela divisão de cebolas_na_caixa por espaco_caixa
caixas_necessarias = cebolas_fora_da_caixa / espaco_caixa # atribui à variável caixas_necessarias o resultado da divisão entre cebolas_fora_da_caixa e espaco_caixa

print("Existem", cebolas_na_caixa, "cebolas encaixotadas") # imprime uma cadeiade caracteres juntamente com o valor armazenado na variável cebolas_na_caixa
print("Existem", cebolas_fora_da_caixa, "cebolas sem caixa") # imprime uma cadeiade caracteres juntamente com o valor armazenado na variável cebolas_fora_da_caixa
print("Em cada caixa cabem", espaco_caixa, "cebolas") # imprime uma cadeiade caracteres juntamente com o valor armazenado na variável espaco_caixa
print("Ainda temos,", caixas_vazias, "caixas vazias") # imprime uma cadeiade caracteres juntamente com o valor armazenado na variável caixas_vazias
print("Então, precisamos de", caixas_necessarias, "caixas para empacotar todas as cebolas") # imprime uma cadeiade caracteres juntamente com o valor armazenado na variável caixas_necessarias