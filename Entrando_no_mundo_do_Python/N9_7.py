def str_to_sorted_list(text):
	text = list(text)
	text.sort()
	return text

def identificar_anagramas(palavra, lista):
	anagramas = []
	palavra = str_to_sorted_list(palavra)
	
	for i in lista:
		tester = str_to_sorted_list(i)
		if tester == palavra:
			anagramas.append(i)

	return anagramas

lista_de_palavras = ["cebola", "carro", "oi", "mansões"]
palavra = "corra"

anagramas = identificar_anagramas(palavra, lista_de_palavras)

for x in anagramas:
	print(x)