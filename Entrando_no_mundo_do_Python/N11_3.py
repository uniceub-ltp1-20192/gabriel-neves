pizza_hut = ["calabresa", "frango com catupiry", "quatro queijos", "portuguesa"]
pizza_hot_paraguai = pizza_hut[:]
pizza_hot_paraguai.append("banana com canela")

print("Pizza Hut:")
for sabor in pizza_hut:
	print("\t" + sabor)

print("\nPizza Hot Paraguai:")
for sabor in pizza_hot_paraguai:
	print("\t" + sabor)