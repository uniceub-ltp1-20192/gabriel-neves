import re

class Validator:
    
    @staticmethod
    def getId(txt):
        while True:
            num = input(txt)
            match = re.match("\d+", num)
            if match:
                return num
            else:
                print("Opção inválida! Infome um valor inteiro.")
    
    @staticmethod
    def validateMenuOp(expReg):
        while True:
            op = input("O que deseja fazer? ")
            v  = re.match(expReg, op)
            if v != None:
                return op
            else:
                print("Opção inválida! Infome um valor no intervalo {}.".format(expReg))

    
    @staticmethod
    def getTitle():
        while True:
            title = input("Titulo: ")
            
            if title == "":
                print("\nPor favor insira um título válido.")
            else:
                return title.lower()

    @staticmethod
    def getDuration():
        while True:
            duration = input("Duração em minutos: ")
            duration = re.sub("\D+", "", duration)
            
            if Validator.isValidDuration(duration):
                return int(duration)
            else:
                print("\nPor favor insira uma duração válida.")

    @staticmethod
    def getResolution():
        while True:
            resolution = input("Resolução (largura X altura): ")
            resolution = resolution.lower()
            resolution = re.findall("[\dx]", resolution)
            
            if Validator.isValidResolution(resolution):
                return "".join(resolution)
            else:
                print("\nPor favor insira uma resolução válida.")

    @staticmethod
    def getGender():
        while True:
            gender = input("Gênero: ")
            
            if gender == "":
                print("\nPor favor insira um gênero válido.")
            else:
                return gender.lower()

    @staticmethod
    def getParentalRating():
        while True:
            pr = input("Classificação indicativa: ")
            
            if pr == "":
                print("\nPor favor insira uma classificação válido.")
            else:
                return pr.lower()
    
    
    @staticmethod
    def isValidDuration(duration):
        if (duration == ""
            or int(duration) <= 0):
            return False

        return True

    @staticmethod
    def isValidResolution(resolution):
        if (resolution == []
            or not "x" in resolution
            or resolution[0]  == "x"
            or resolution[-1] == "x"):
            return False

        return True
