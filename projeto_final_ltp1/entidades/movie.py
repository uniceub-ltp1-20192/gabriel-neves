from entidades.video import Video

class Movie(Video):
    def __init__(self, video_id, title, duration, resolution, gender, parentalRating):
        super().__init__(video_id, title, duration, resolution)
        self._gender         = gender
        self._parentalRating = parentalRating

    @property
    def gender(self):
        return self._gender
    
    @gender.setter
    def gender(self, gender):
        self._gender = gender

    
    @property
    def parentalRating(self):
        return self._parentalRating
    
    @parentalRating.setter
    def parentalRating(self, parentalRating):
        self._parentalRating = parentalRating

    
    def hasAttribute(self, attribute):
        attributesList = [self._gender, self._parentalRating]
        
        if super().hasAttribute(attribute) or attribute in attributesList:
            return True
        return False

    def valuesToBePrinted(self):
        return """\tGênero    - {}
        C.I.      - {}\n""".format(self._gender, self._parentalRating)
        
    def printValues(self):
        print(super().valuesToBePrinted() + self.valuesToBePrinted())
        
    def txtFileFormat(self):
        return super().txtFileFormat() + ";{};{}".format(self._gender.title(), self._parentalRating)