class Video:
    def __init__(self, video_id, title, duration, resolution):
        self._id         = video_id
        self._title      = title
        self._duration   = duration
        self._resolution = resolution

    @property
    def id(self):
        return self._id
    
    @id.setter
    def id(self, video_id):
        self._id = video_id

    
    @property
    def title(self):
        return self._title
    
    @title.setter
    def title(self, title):
        self._title = title

    
    @property
    def duration(self):
        return self._duration
    
    @duration.setter
    def duration(self, duration):
        self._duration = duration


    @property
    def resolution(self):
        return self._resolution
    
    @resolution.setter
    def resolution(self, resolution):
        self._resolution = resolution

    
    def readableTime(self, min):
        if min//60 != 0:
            hours = "{} hora".format(min//60)
            if min//60 > 1:
                hours += "s"
        else:
            hours = ""
        
        minutes = min%60
        if minutes != 0:
            min = "{} minuto".format(minutes)
            if minutes:
                min += "s"
        else:
            min = ""

        if hours == "" or min == "":
            return hours + min
        return hours + " e " + min

    def hasAttribute(self, attribute):
        attributesList = [self._id, self._title, str(self._duration), self._resolution]
        
        if attribute in attributesList:
            return True
        return False

    def valuesToBePrinted(self):
        return """
        ID        - {}
        Título    - {}
        Duração   - {}
        Resolução - {}\n""".format(self._id[1:], self._title.title(), self.readableTime(self.duration), self._resolution)
        
    def printValues(self):
        print(self.valuesToBePrinted())

    def txtFileFormat(self):
        return "{};{};{};{}".format(self._id, self._title, self._duration, self._resolution)