from menu.menu import Menu
from dados.dados import Data

import os

if os.path.exists("data.txt"):
    data = Data(Data.readFile("data.txt"))
else:
    data = Data()

data = Menu.mainMenu(data)

data.saveFile("data.txt")