from dados.dados import Data
from validador.validador import Validator

from os import system
import re

class Menu:
    @classmethod
    def mainMenu(cls, data):
        while True:
            system('cls')
            op = cls.menuOp()

            if op == "0":
                print("\nTchau.\n")
                return data
            
            elif op == "1":
                cls.consultMenu(data)

            elif op == "2":
                data = cls.insertMenu(data)

            elif op == "3":
                data = cls.changeMenu(data)

            elif op == "4":
                data = cls.exclusionMenu(data)        

    @staticmethod
    def menuOp():
        print("MENU PRINCIPAL\n\n0 – Sair\n1 – Consultar\n2 – Inserir\n3 – Alterar\n4 – Excluir\n")
        return Validator.validateMenuOp("[0-4]")
    
    
    @classmethod
    def consultMenu(cls, data, menuDetail=""):
        system('cls')
        if data.isEmpty():
            print("Não há nada armazenado.\n")
            return input("continuar...\n")

        while True:
            op = cls.consultMenuOp(menuDetail)

            if op == "1":
                identifier = Validator.getId("\nDigite o identificador: ")
                data.consultId(identifier) 

            elif op == "2":
                attribute = input("\nDigite o atributo: ")
                data.consultAttribute(attribute.lower())  

            if op == "0" or menuDetail != "":
                break
            
            system('cls')

    @staticmethod
    def consultMenuOp(menuDetail):
        print("MENU PRINCIPAL -> {}CONSULTA\n\n0 – Voltar\n1 – Consultar por Identificador\n2 – Consultar por Atributo\n".format(menuDetail) )
        return Validator.validateMenuOp("[0-2]")
    
    
    @classmethod
    def insertMenu(cls, data):
        system('cls')
        while True:
            system('cls')
            op = cls.insertMenuOp()
            system('cls')

            if op == "0":
                return data
            
            elif op == "1":
                print("MENU PRINCIPAL -> INSERÇÃO -> VÍDEO\n")
                data.add()

            elif op == "2":
                print("MENU PRINCIPAL -> INSERÇÃO -> FILME\n\n")
                data.add(movie=True)
            
            print("\nDados armazenados.")
            input("continuar...")

    @staticmethod
    def insertMenuOp():
        print("MENU PRINCIPAL -> INSERÇÃO\n\n0 – Voltar\n1 – Inserir vídeo\n2 – Inserir filme\n")
        return Validator.validateMenuOp("[0-2]")
    
    
    @classmethod
    def changeMenu(cls, data):
        system('cls')
        if data.isEmpty():
            print("Não há nada armazenado.\n")
            input("continuar...\n")
            return data
        
        op = "2"
        while True:
            if op == "2":
                cls.consultMenu(data, "ALTERAÇÃO -> ")
            
            op = cls.changeMenuOp()

            if op == "0":
                return data
            
            elif op == "1":
                identifier = Validator.getId("\nDigite o identificador do item a ser alterado: ")
                system('cls')
                data.change(identifier)
            
            system('cls')

    @staticmethod
    def changeMenuOp():
        print("MENU PRINCIPAL -> ALTERAÇÃO\n\n0 – Voltar\n1 – Alterar\n2 – Consultar\n")
        return Validator.validateMenuOp("[0-2]")


    @classmethod
    def exclusionMenu(cls, data):
        system('cls')
        if data.isEmpty():
            print("Não há nada armazenado.\n")
            input("continuar...\n")
            return data
        
        op = "2"
        while True:
            if op == "2":
                cls.consultMenu(data, "EXCLUSÃO -> ")
            
            op = cls.exclusionMenuOp()

            if op == "0":
                return data
            
            elif op == "1":
                identifier = Validator.getId("\nDigite o identificador do item a ser excluído: ")
                data.exclude(identifier)
            
            system('cls')

    @staticmethod
    def exclusionMenuOp():
        print("MENU PRINCIPAL -> EXCLUSÃO\n\n0 – Voltar\n1 – Excluir\n2 – Consultar\n")
        return Validator.validateMenuOp("[0-2]")