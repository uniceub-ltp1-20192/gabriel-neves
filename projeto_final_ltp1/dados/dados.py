from validador.validador import Validator
from entidades.video import Video
from entidades.movie import Movie

class Data:
    def __init__(self, Dict=dict()):
        self._dict = Dict

    @property
    def Dict(self):
        return self._dict

    def add(self, movie=False):
        video_id   = self.keyGenerator()
        title      = Validator.getTitle()
        duration   = Validator.getDuration()
        resolution = Validator.getResolution()
        
        if movie:
            gender     = Validator.getGender()
            parentalRa = Validator.getParentalRating()
            self._dict[video_id] = Movie("m"+video_id, title, duration, resolution, gender, parentalRa)

        else:
            self._dict[video_id] = Video("v"+video_id, title, duration, resolution)

        self._dict[video_id].printValues()
            

    def change(self, item):
        if item in self._dict:
            print("\nValores originais:\n")
            self._dict[item].printValues()
            print("Novos valores:\n")
            title      = Validator.getTitle()
            duration   = Validator.getDuration()
            resolution = Validator.getResolution()
            
            if isinstance(self._dict[item], Movie):
                gender     = Validator.getGender()
                parentalRa = Validator.getParentalRating()
                self._dict[item] = Movie("m"+item, title, duration, resolution, gender, parentalRa)

            else:
                self._dict[item] = Video("v"+item, title, duration, resolution)

            self._dict[item].printValues()
            print("\nItem modificado.")
        
        else:
            print("O item informado não existe.")
        
        input("continuar...\n")
    
    def exclude(self, item):
        if item in self._dict:
            self._dict.pop(item)
            print("\nItem excluído.")
        else:
            print("\nO item informado não existe.")
        
        input("continuar...\n")

    def consultId(self, id):
        if id in self._dict:
            self._dict[id].printValues()
        else:
            print("Nada encontrado.\n")

        input("continuar...\n")

    def consultAttribute(self, attribute):
        notFound = True
        
        for x in self._dict:
            if self._dict[x].hasAttribute(attribute):
                self._dict[x].printValues()
                notFound = False

        if notFound:
            print("Nada encontrado.\n")

        input("continuar...\n")


    
    def keyGenerator(self):
        key = 0
        while str(key) in self._dict:
            key += 1
        return str(key)

    def isEmpty(self):
        if len(self._dict) == 0:
            return True
        return False

    @staticmethod
    def readFile(fileName):
        file = open(fileName, "r")
        data = dict()
        
        for line in file:
            if line[0] == "v":
                c = line.split(";")
                data[c[0][1:]] = Video(c[0], c[1], int(c[2]), c[3])
            elif line[0] == "m":
                c = line.split(";")
                data[c[0][1:]] = Movie(c[0], c[1], int(c[2]), c[3], c[4], c[5])

        file.close()
        return data

    def saveFile(self, fileName):
        file = open(fileName, "w")
        for item in self._dict:
            file.write( self._dict[item].txtFileFormat() + "\n" )
        file.close