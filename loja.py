def sum_prices(sum_no_discount, sum_discount, product_number):
    while True:
        entry = input("\nQual o valor do produto " + str(product_number) + "? ")
        price = float(entry.replace(",", "."))

        if price > 0:
            break

        print("Valor inválido! Tente novamente com um número positivo.")

    while True:
        entry = input("Quanto será dado de desconto no produto " + str(product_number) + "? (0-100) ")
        discount = float(entry.replace(",", "."))

        if 0 <= discount <= 100:
            break

        print("Valor inválido! Tente novamente com um número entre 0 e 100.")

    return sum_no_discount + price, sum_discount + price * (100 - discount) / 100


def ask_yes_or_no(text):
    result = input(text)
    affirmative = ["sim", "s", "si", "yes", "y", "ye", ""]
    if result.lower() in affirmative:
        return True

    return False


def print_results(a, b):
    print("\n_________________________________")
    print("Você pagaria:   R$", round(a, 2))
    print("Você vai pagar: R$", round(b, 2))
    return


total = 0
total_with_discount = 0
current_product = 1
add_products = True

while add_products:
    try:
        total, total_with_discount = sum_prices(total, total_with_discount, current_product)
        add_products = ask_yes_or_no("\nDeseja adicionar um novo produto? ")
        current_product += 1

    except ValueError:
        print("\nValor inválido! Tente novamente com um número.")

print_results(total, total_with_discount)
