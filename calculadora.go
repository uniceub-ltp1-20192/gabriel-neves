package main

import ("fmt"
"strconv"
"time")

func main() {

	number_one := askNumber("Digite primeiro nº: ")

	operation  := askString("Operação: ") 

	number_two := askNumber("Digite segundo nº: ")

	if (!isValidNumber(number_one) || !isValidNumber(number_two) || !isValidOperation(operation)) { 
		fmt.Print("Valores inválidos!")
		time.Sleep(3 * time.Second)
		return 
	}

	result := calculate(number_one, number_two, operation)

	printResult(result)
	time.Sleep(3 * time.Second)
	return
}

func askString(msg string) string {

	// declara a var input
	var input string

	// apresenta a string on-the-face
	fmt.Print(msg)
    
    // lê o que o usuário digitou
    fmt.Scanln(&input)

    return input
}

func askNumber(msg string) float32 {

	// declara a var input
	var input float32

	// apresenta a string Nome on-the-face
	fmt.Print(msg)
    
    // lê o que o usuário digitou
    fmt.Scanln(&input)

    return input
}

func calculate(n float32, m float32, op string) float32 {

	if (op == "+") {
		return n + m
	}

	if (op == "-") {
		return n - m
	}

	if (op == "*") {
		return n * m
	}

	return n / m
}

func printResult(number interface{}) {

	fmt.Printf("O resultado é: %v \n", number)
}

func isValidNumber(n float32) bool {
	nf := float64(n)
	var s string
	s = strconv.FormatFloat(nf, 'E', -1, 64)
	_, err := strconv.ParseFloat(s, 64)
	return err == nil
}

func isValidOperation(n string) bool {
	return n == "+" || n == "-" || n == "*" || n == "/"
}
